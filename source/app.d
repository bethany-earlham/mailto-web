import core.runtime;
import core.sys.windows.windows;

/* Windows 10 resets the default mail handler if I change it. So this provides
   an intermediary; we just pass the mailto data along to the default browser.
*/

extern (Windows)
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
            LPSTR lpCmdLine, int nCmdShow)
{
    int result;

    try {
        Runtime.initialize();
        result = myWinMain(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
        Runtime.terminate();
    } catch (Throwable e) {
        import std.string : toStringz;
        MessageBoxA(null, e.toString().toStringz(), null,
                    MB_ICONEXCLAMATION);
        result = 0;     // failed
    }

    return result;
}

int myWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
              LPSTR lpCmdLine, int nCmdShow) {
    import std.string : fromStringz, replace;
    import std.process : browse;
    auto args = lpCmdLine.fromStringz.replace("mailto:", "");
    browse("https://zimbra.earlham.edu/?view=compose&to=" ~ args);
    return 0;
}
